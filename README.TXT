A clone of django chronograph 
(https://bitbucket.org/wnielson/django-chronograph/) 
that removes the date compatibility module. 

I faced issues with django-chronographs use of date data types. 
It had bugs which led to cron jobs being scheduled incorrectly 
because of time zone issues. It was also not entirely compatible 
with Python 2.6. 

Removing the date compatibility module does not 
take much away. If the OS, Django and database all use time zones 
and are set up properly, the rest of the module would work just 
fine without needing any complicated date transformation layer. 

It is not in the form of a Python installable module yet. But 
you can plonk this into your application and get it working.

